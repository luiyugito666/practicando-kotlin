package com.example.user.aplicacionmovil

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.user.aplicacionmovil.R.id.textView
import kotlinx.android.synthetic.main.activity_main.*






@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), TextWatcher {


    private var editName: EditText? = null
    private var editAge: EditText? = null
    private var textName :TextView? = null
    private var textAge :TextView? = null
    private var button :Button?= null
    private var name: String? = null
    private var age: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editName = findViewById(R.id.editText_Name)
        editAge = findViewById(R.id.editText_Age)
        textName= findViewById(R.id.textView_Name)
        textAge = findViewById(R.id.textView_Age)
        button = findViewById(R.id.button_Ejecutar)

        editName?.addTextChangedListener(this) // se implementa el watcher para crear
        editAge?.addTextChangedListener(this)//3 funciones que se ejecutaran los cambio
        //antes durante y despues de la ejecucion de ingreso de textos

    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        name = editName?.text.toString()
        age = editAge?.text.toString()

        if (name != "") textName?.text = name
        if (age!= "") textAge?.text= age





      //Toast.makeText(this,s.toString(),Toast.LENGTH_SHORT).show()
        //toast es para mostrar un texto sombreado
        //Toast.LENGTH_SHORT es para ver el tramaño del texto
        //show() es para visualizar
    }

}



